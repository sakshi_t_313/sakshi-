from flask import Flask, render_template
from flask_sqlalchemy import SQLAlchemy

app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///test.db'

db = SQLAlchemy(app)

class Task(db.Model):
  id = db.Column(db.Integer, primary_key=True)
  text = db.Column(db.Text)
  complete = db.Column(db.Boolean, default=False)
 
@app.route('/')
def index():
    return render_template('index.html')

if __name__ == '__main__':
    app.run(debug=True)




'''from flask import Flask, render_template
app = Flask(__name__)
@app.route('/')
def index():
	return render_template('shi.html')
@app.route('/it')
def index1():
	return render_template('add.html')
@app.route('/itt')
def index2():
	return render_template('index.html')
@app.route('/itt')
def index3():
	return render_template('sak.html')
@app.route('/itts')
def index4():
	return render_template('++.html')
@app.route('/ittss')

def index5():
	return render_template('heyy.html')
if __name__ == '__main__':
	app.run(debug=True)'''
